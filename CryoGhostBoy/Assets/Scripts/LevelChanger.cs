﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour
{
    public Animator animator;

    private string levelToLoad;

    public string Loading;

    public PressurePlate NextRoom;
    public GameObject player;

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
          FadeToLevel(Loading);
        }

        if (NextRoom.levelComplete && player.transform.position.x > 1.35f)
        {
            FadeToLevel(Loading);
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            FadeToLevel(Loading);
        }
    }

    public void FadeToLevel (string levelIndex)
    {
        levelToLoad = levelIndex;
        animator.SetTrigger("FadeOut");
    }

    public void OnFadeComplete()
    {
        Application.LoadLevel(Loading);
    }
}
