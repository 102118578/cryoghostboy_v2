﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanR : MonoBehaviour
{
    public GameObject player;

    bool fanOn = false;

    public GameObject pickUpParent;

    public bool isHolding = false;

    public float interactRange;

    public Animator animator;

    public GameObject TurnOn;
    public GameObject CanPossess;

    // Start is called before the first frame update
    void Start()
    {
        animator.SetBool("FanOn", fanOn);
    }

    // Update is called once per frame
    void Update()
    {
        if (fanInRange(interactRange))
        {
            transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().enabled = true;

            if (Input.GetKeyDown(KeyCode.O) &&  isHolding == true)
            {
                fanOn = !fanOn;
                animator.SetBool("FanOn", fanOn);
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                isHolding = !isHolding;
            }
        }
        else
        {
            transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }

        if (isHolding)
        {
            player.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,0f);
            player.transform.position = transform.position;
            transform.parent = pickUpParent.transform;
            //player.GetComponent<SpriteRenderer>().enabled = false;
        }
        else
        {
            transform.parent = null;
            //player.GetComponent<SpriteRenderer>().enabled = true;
            player.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,0.8f);
        }

        if (fanOn)
        {
            GetComponent<AreaEffector2D>().enabled = true;
        }
        else
        {
            GetComponent<AreaEffector2D>().enabled = false;
        }

        //Text - O to push
        if (isHolding == true)
        {
            TurnOn.GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            TurnOn.GetComponent<SpriteRenderer>().enabled = false;
        }


    }

    public bool fanInRange(float offset)
    {
        bool result = false;
        if (player.transform.position.x > transform.position.x - offset && player.transform.position.x < transform.position.x + (0.4f * transform.localScale.x) + offset)
        {
            result = true;

           CanPossess.GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            CanPossess.GetComponent<SpriteRenderer>().enabled = false;
        }


        return result;
    }
}
