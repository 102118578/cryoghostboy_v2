using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatClimbBox : MonoBehaviour
{
    public GameObject melon;

    public void OnTriggerEnter2D(Collider2D collision)
    {
      if (collision.tag == "Box")
      {
        GetComponent<Rigidbody2D>().AddForce(new Vector3(-1.8f, 4.2f, 0.0f), ForceMode2D.Impulse);
      }
      if (collision.tag == "NextLevelTrigger")
      {
        melon.GetComponent<Rigidbody2D>().AddForce(new Vector3(1, 0, 0), ForceMode2D.Impulse);
      }
    }
}
