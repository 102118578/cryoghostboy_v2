﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementFanLevel : MonoBehaviour
{
    public FanR fanR;
    public FanL fanL;

    public GameObject player;

    public float runSpeed;

    int dy;
    int dx;

    public Animator animator;

    void FixedUpdate()
    {
        dy = 0;
        dx = 0;

        if (Input.GetKey(KeyCode.W))
        {
            dy += 1;
        }
        if (Input.GetKey(KeyCode.A))
        {
            dx += -1;
        }
        if (Input.GetKey(KeyCode.S))
        {
            dy += -1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            dx += 1;
        }

        animator.SetFloat("dx", dx);

        Vector3 targetPos = player.transform.position + new Vector3(dx * runSpeed, dy * runSpeed, 0);

        if (fanR != null)
        {
            if (fanR.isHolding || fanL.isHolding)
            {
                if (targetPos.x > 1.4f)
                {
                    targetPos.x = 1.4f;
                }
                if (targetPos.x < -1.4f)
                {
                    targetPos.x = -1.4f;
                }
                if (targetPos.y > 0.65f)
                {
                    targetPos.y = 0.65f;
                }
                if (targetPos.y < -0.53f)
                {
                    targetPos.y = -0.53f;
                }
            }
            else
            {
                if (targetPos.x > 1.62f)
                {
                    targetPos.x = 1.62f;
                }
                if (targetPos.x < -1.62f)
                {
                    targetPos.x = -1.62f;
                }
                if (targetPos.y > -0.35f)
                {
                    targetPos.y = -0.35f;
                }
                if (targetPos.y < -0.35f)
                {
                    targetPos.y = -0.35f;
                }
            }
        }
        else
        {
            if (targetPos.x > 1.62f)
            {
                targetPos.x = 1.62f;
            }
            if (targetPos.x < -1.62f)
            {
                targetPos.x = -1.62f;
            }
            if (targetPos.y > -0.35f)
            {
                targetPos.y = -0.35f;
            }
            if (targetPos.y < -0.35f)
            {
                targetPos.y = -0.35f;
            }
        }

        player.transform.position = targetPos;
    }
}
