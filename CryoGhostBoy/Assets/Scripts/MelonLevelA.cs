using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MelonLevelA : MonoBehaviour
{
    public GameObject oven;
    public Oven ovenScript;

    public GameObject smoke;
    public GameObject smokeAlarmTrigger;

    public bool melonInOven = false;

    // Start is called before the first frame update
    void Start()
    {
      smoke.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x > oven.transform.position.x - 0.15f && transform.position.x < oven.transform.position.x + 0.25f && ovenScript.doorOpen)
        {
          //Melon falls into oven
          GetComponent<CircleCollider2D>().enabled = false;
          if(transform.position.y < -0.5f)
          {
            GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            GetComponent<Rigidbody2D>().gravityScale = 0;
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;

            melonInOven = true;
          }
        }

        if (melonInOven && ovenScript.ovenOn && !ovenScript.doorOpen)
        {
            smoke.SetActive(true);
            GetComponent<Renderer>().material.color = Color.gray;
            smokeAlarmTrigger.GetComponent<Rigidbody2D>().AddForce(new Vector3(0, 0.1f, 0));
        }
    }
}
