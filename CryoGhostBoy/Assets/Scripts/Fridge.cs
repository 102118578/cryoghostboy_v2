using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Fridge : MonoBehaviour
{
    public GameObject player;
    public GameObject cat;

    public GameObject fridge;

    bool fridgeOn = false;

    public bool isHolding = false;

    public float interactRange;

    public float fridgePushSpeed;

    public GameObject CanPossessFridge;
    public GameObject FridgeOn;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (fridgeInRange(interactRange))
        {
            transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().enabled = true;

            if (Input.GetKeyDown(KeyCode.O) && isHolding == true)
            {
                fridgeOn = !fridgeOn;
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                isHolding = !isHolding;
            }
        }
        else
        {
            transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }

        if (isHolding)
        {
            player.GetComponent<SpriteRenderer>().sortingLayerName = "Background";
            player.transform.position = transform.position;
        }
        else
        {
            player.GetComponent<SpriteRenderer>().sortingLayerName = "Foreground";
        }

        if (isHolding ==true)
        {
            FridgeOn.GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            FridgeOn.GetComponent<SpriteRenderer>().enabled = false;
        }

        if (fridgeOn)
        {
            if (SceneManager.GetActiveScene().name == "Tutorial2")
            {
                cat.transform.position = Vector3.MoveTowards(cat.transform.position, new Vector3(0.92f, -0.575f, 0.0f), fridgePushSpeed);
            }
            else if (SceneManager.GetActiveScene().name == "LevelA" && cat.transform.position.x > -0.29f)
            {
                cat.transform.position = Vector3.MoveTowards(cat.transform.position, new Vector3(-0.29f, -0.575f, 0.0f), fridgePushSpeed);
            }
        }

    }

    public bool fridgeInRange(float offset)
    {
        bool result = false;

        if (player.transform.position.x > transform.position.x - offset && player.transform.position.x < transform.position.x + offset)
        {
            result = true;
            CanPossessFridge.GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            CanPossessFridge.GetComponent<SpriteRenderer>().enabled = false;
        }

        return result;
    }
}
