using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oven : MonoBehaviour
{
    public GameObject player;

    public bool ovenOn = false;
    public bool doorOpen = false;
    bool isHolding = false;

    public float interactRange;

    public Animator stateAnimator;
    public Animator doorAnimator;

    public GameObject CanPossessOven;
    public GameObject OvenOn;

    // Start is called before the first frame update
    void Start()
    {
        stateAnimator.SetBool("OvenOn", ovenOn);
        doorAnimator.SetBool("DoorOpen", doorOpen);
    }

    // Update is called once per frame
    void Update()
    {
        if (ovenInRange(interactRange))
        {
            transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().enabled = true;

            if (Input.GetKeyDown(KeyCode.O) && isHolding)
            {
                ovenOn = !ovenOn;
                stateAnimator.SetBool("OvenOn", ovenOn);
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                isHolding = !isHolding;
            }

            if (Input.GetKeyDown(KeyCode.I) && isHolding)
            {
                doorOpen = !doorOpen;
                doorAnimator.SetBool("DoorOpen", doorOpen);
            }
        }
        else
        {
            transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }

        if (isHolding)
        {
            player.GetComponent<Animator>().enabled = false;
            player.GetComponent<SpriteRenderer>().enabled = false;
            player.transform.position = transform.position;
        }
        else
        {
            player.GetComponent<Animator>().enabled = true;
            player.GetComponent<SpriteRenderer>().enabled = true;
        }

        if (isHolding == true)
        {
            OvenOn.GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            OvenOn.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    public bool ovenInRange(float offset)
    {
        bool result = false;

        if (player.transform.position.x > transform.position.x - offset && player.transform.position.x < transform.position.x + offset)
        {
            result = true;
            CanPossessOven.GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            CanPossessOven.GetComponent<SpriteRenderer>().enabled = false;
        }

        return result;
    }
}
