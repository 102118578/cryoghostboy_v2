﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeTut1 : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "KnifeTrigger")
        {
            GetComponent<Rigidbody2D>().gravityScale = 0.6f;
        }
    }
}
