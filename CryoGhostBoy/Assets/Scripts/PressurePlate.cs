﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PressurePlate : MonoBehaviour
{
    public GameObject door;
    public GameObject doorLightGreen;
    public GameObject doorLightRed;
    public GameObject player;

    public string SceneName;
    public string LevelToLoad;

    public bool levelComplete;

    //public void Update()
    //{
    //    if (levelComplete && player.transform.position.x > 1.35f)
    //    {
    //        // TODO next level
    //        if (SceneManager.GetActiveScene().name == SceneName)
    //        {
    //            SceneManager.LoadScene(LevelToLoad);
    //        }
    //    }
    //}

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "NextLevelTrigger")
        {
            levelComplete = true;
            door.GetComponent<SpriteRenderer>().enabled = false;
            doorLightGreen.GetComponent<SpriteRenderer>().enabled = true;
            doorLightRed.GetComponent<SpriteRenderer>().enabled = false;
        }
    }
}
